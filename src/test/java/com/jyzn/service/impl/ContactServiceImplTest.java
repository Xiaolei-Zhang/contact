package com.jyzn.service.impl;

import com.jyzn.mapper.ContactMapper;
import com.jyzn.pojo.Contact;
import com.jyzn.service.ContactService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContactServiceImplTest {

    @Resource
    private ContactService contactService;

    @Resource
    private ContactMapper contactMapper;

    @Test
    public void save() {
        Contact contact = new Contact();
        contact.setCompany("123");
        contact.setName("小张");
        contact.setDescription("11111");
        boolean save = contactService.save(contact);
        System.out.println(save);
    }

    @Test
    public void add(){
        Contact contact = new Contact();
        contact.setCompany("123");
        contact.setName("小张");
        contact.setDescription("11");
        Integer add = contactService.add(contact);
        System.out.println(add);
    }

    @Test
    public void add1(){
        Contact contact = new Contact();
        contact.setCompany("123");
        contact.setName("小张");
        contact.setDescription("11111");
        int insert = contactMapper.insert(contact);
//        int i = contactMapper.deleteById(1);
        System.out.println(insert);
    }
}