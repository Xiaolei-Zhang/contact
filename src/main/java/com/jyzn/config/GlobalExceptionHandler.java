package com.jyzn.config;

import com.jyzn.pojo.bo.Result;
import com.jyzn.utils.LogicException;
import com.jyzn.utils.ResultUtil;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Result> feignException(FeignException e) {
        int status = e.status();
        return ResultUtil.error(e.getMessage()).toResponseEntity(HttpStatus.valueOf(status));
    }

    @ExceptionHandler(LogicException.class)
    public ResponseEntity<Result> logicException(LogicException e) {
        return ResultUtil.error(e.getMessage()).toResponseEntity(e.getStatus());
    }

}
