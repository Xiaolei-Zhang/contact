package com.jyzn.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class Constants {

    public class ConfigData {
        //TODO 需要改为对应项目的mapper所在包名
        public static final String MAPPER_PACKAGE = "com.jyzn.mapper";
    }

    public static final String BUSINESS_ID = "IntelligentAudio";

    public class HttpHeader {
        public static final String KEY_ACCESS_TOKEN = "Authorization";
    }

    public class TTS {
        public static final String HOST_URL = "https://tts-api.xfyun.cn/v2/tts";//http url 不支持解析 ws/wss schema
        public static final String API_KEY = "b77410b278f3dd28a81af54144da5f2e";
        public static final String API_SECRET = "5ccbc7e7c1f7e9f2e064b100c493e443";
        public static final String APP_ID = "604ede11";
    }

    public class OSS {
        public static final String REGION = "oss-cn-hangzhou";

        public static final String END_POINT = "http://" + REGION + ".aliyuncs.com";

        public static final String ACCESS_KEY_ID = "LTAI4GCC53mSxEgEEFpuVr3g";

        public static final String ACCESS_KEY_SECRET = "87dfSVsAsZUc6zdFwCNo66J1qQX4Aj";

        public static final String URL_BASE = "http://oss.touyue8.com/";

        public static final String STS_END_POINT = "sts.aliyuncs.com";

        public static final String STS_ACCESS_KEY_ID = "LTAI4G7uiWzLiLWvMEmJXBUy";

        public static final String STS_ACCESS_KEY_SECRET = "NT5y37wzpQA6ebgEBhmg8KrRUvQZ5G";

        public static final String ROLE_SESSION_NAME = "intelligentAudio";

        public static final String ARN = "acs:ram::1143553195478626:role/ossmanager";

        public static final long DURATION_SECONDS = 3600;

        public static final String BUCKET = "jieyin";
    }
    public class TencentAi {
        public static final String APP_ID = "2156886429";
        public static final String APP_KEY = "5CGDMNIhXg1nkEhG";
        public static final String APP_URL = "https://api.ai.qq.com/fcgi-bin/nlp/nlp_textchat";
    }

    public class BDOCR {
        public static final String GRANT_TYPE = "client_credentials";
        public static final String API_KEY = "oQzEQt7uHeYH4a4tscBIPwQ9";
        public static final String SECRET_KEY = "d909VTaAvj4ejRQnPeG3mk4tcuUoW4LG";
    }

    @Component
    public static class Profile {
        public static String businessCode;

        @Value("${businessCode}")
        public void setBusinessCode(String businessCode) {
            Profile.businessCode = businessCode;
        }
    }


}
