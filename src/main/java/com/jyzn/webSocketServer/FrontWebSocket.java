package com.jyzn.webSocketServer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.jyzn.pojo.NlgAnswer;
import com.jyzn.pojo.QingYunKe;
import com.jyzn.service.NluService;
import com.jyzn.service.feign.QingYunKeService;
import com.jyzn.utils.JsonUtils;
import com.jyzn.utils.TtsTool;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@ServerEndpoint(value = "/websocket")
@Component
public class FrontWebSocket {

    private static TtsTool ttsTool;

    @Autowired
    public void setTtsTool(TtsTool ttsTool) {
        FrontWebSocket.ttsTool = ttsTool;
    }

    private static NluService nluService;

    @Autowired
    public void setNluService(NluService nluService) {
        FrontWebSocket.nluService = nluService;
    }

    private static QingYunKeService qingYunKe;

    @Autowired
    public void setQingYunKe(QingYunKeService qingYunKe) {
        FrontWebSocket.qingYunKe = qingYunKe;
    }

    private final static Map<String, Session> sessionMap = new HashMap<>();
    private final static ObjectMapper mapper = new ObjectMapper();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session) {
        log.info("连接成功");
        sessionMap.put(session.getId(), session);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        sessionMap.remove(session.getId());
        log.info("连接退出");
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        String deal = deal(message);
        sendText(session,deal);
        ttsTool.textToVoice(session,deal);
        System.out.println("收到提问"+message);
        System.out.println("返回文本"+deal);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    public static String deal(String message){
        NlgAnswer nlgAnswer = new NlgAnswer();
        try {
            nlgAnswer = nluService.nlgResponse(message);
        }catch (Exception e){
            e.printStackTrace();
        }

        String reply = "";
        if (nlgAnswer != null && nlgAnswer.getData() != null && nlgAnswer.getData().getReply() != null) {
            reply = nlgAnswer.getData().getReply();
        }
        if ("".equals(reply)){
            try{
                String qingYunKeMes = qingYunKe.qingYunKe("free", 0, message);
                Type type = new TypeToken<QingYunKe>() {
                }.getType();
                QingYunKe qingYunKe = JsonUtils.fromJson(qingYunKeMes, type);
                String resMes = qingYunKe.getContent();
                reply = resMes;
            }catch (Exception e){
                e.printStackTrace();
                reply = "我不理解你在说什么";
            }
        }
        return reply;
    }
    /**
     * 服务端发送消息给客户端
     */
    public static void sendText(Session session, String text) {

        Map<String, Object> map = new HashMap<>();
        map.put("data", text);
        map.put("type", 0);
        session.getAsyncRemote().sendText(JsonUtils.toJson(map));

    }

    public static void sendTTS(Session session, String url) {

        Map<String, Object> map = new HashMap<>();

        map.put("data", url);
        map.put("type", 1);
        session.getAsyncRemote().sendText(JsonUtils.toJson(map));

    }

    public static boolean socketAvailable() {
        boolean available = !sessionMap.isEmpty();
        log.info("session available:{}", available);
        return available;
    }
}