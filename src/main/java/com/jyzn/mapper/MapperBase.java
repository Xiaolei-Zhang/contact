package com.jyzn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface MapperBase<T> extends BaseMapper<T> {
}
