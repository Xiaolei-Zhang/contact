package com.jyzn.mapper;

import com.jyzn.pojo.Contact;

public interface ContactMapper extends MapperBase<Contact> {
}
