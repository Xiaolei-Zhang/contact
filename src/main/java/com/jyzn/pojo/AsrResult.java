package com.jyzn.pojo;

import lombok.Data;

import java.util.List;

@Data
public class AsrResult {

    String corpus_no;

    String err_msg;

    Integer err_no;

    List<String> result;

    String sn;

}
