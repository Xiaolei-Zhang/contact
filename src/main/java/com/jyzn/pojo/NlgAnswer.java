package com.jyzn.pojo;

import lombok.Data;

@Data
public class NlgAnswer {

    private int code;
    private String msg;
    private NlgData data;
    private Boolean success;

}
