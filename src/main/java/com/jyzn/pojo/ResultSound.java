package com.jyzn.pojo;

import lombok.Data;

@Data
public class ResultSound {

    private String requestId;

    private String message;

    private String soundUrl;
}
