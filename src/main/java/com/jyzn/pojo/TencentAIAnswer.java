package com.jyzn.pojo;

import lombok.Data;

@Data
public class TencentAIAnswer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int ret;
    private String msg;
    private DataRepost data;
}
