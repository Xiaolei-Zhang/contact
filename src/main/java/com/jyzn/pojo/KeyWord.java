package com.jyzn.pojo;

import lombok.Data;

import java.util.List;

@Data
public class KeyWord {

    private Integer code;
    private String msg;
    private List<String> data;
    private Boolean success;

}
