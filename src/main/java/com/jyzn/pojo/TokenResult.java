package com.jyzn.pojo;

import lombok.Data;

@Data
public class TokenResult {

    private String refresh_token;

    private Integer expires_in;

    private String session_key;

    private String access_token;

    private String scope;

    private String session_secret;

}
