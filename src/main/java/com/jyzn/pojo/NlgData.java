package com.jyzn.pojo;

import lombok.Data;

import java.util.Map;

@Data
public class NlgData {

    private String intent;
    private String skill;
    private String task;
    private String pattern;
    private String reply;
    private Map slotDataMap;
}
