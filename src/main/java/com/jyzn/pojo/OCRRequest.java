package com.jyzn.pojo;

import lombok.Data;

@Data
public class OCRRequest {

    private String image;

    private String image_url;

    private String language_type;

    private String detect_direction;

    private String detect_language;

    private String probability;
}
