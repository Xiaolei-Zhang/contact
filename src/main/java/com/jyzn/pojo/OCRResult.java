package com.jyzn.pojo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class OCRResult {

    private Integer direction;

    private Long log_id;

    private List<Map<String, String>> words_result;

    private Integer words_result_num;

    private Boolean probability;

    private Integer language;

    private Integer error_code;

    private String error_msg;

}
