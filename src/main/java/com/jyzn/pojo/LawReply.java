package com.jyzn.pojo;

import lombok.Data;

import java.util.Map;

@Data
public class LawReply {

    private Integer code;
    private String msg;
    private Map<String, String> data;
    private Boolean success;

}
