package com.jyzn.pojo;

import lombok.Data;

@Data
public class DataRepost {
    private String session;
    private String answer;
}
