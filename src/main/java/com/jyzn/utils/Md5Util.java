package com.jyzn.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Util {
    /**
     * MD5加密方法
     *
     * @return
     */
    public static String getMD5(String info) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(info.getBytes("UTF-8"));
            byte[] encryption = md5.digest();

            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < encryption.length; i++) {
                if (Integer.toHexString(0xff & encryption[i]).length() == 1) {
                    strBuf.append("0").append(
                            Integer.toHexString(0xff & encryption[i]));
                } else {
                    strBuf.append(Integer.toHexString(0xff & encryption[i]));
                }
            }
            return strBuf.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        } catch (UnsupportedEncodingException e) {
            return "";
        }
        // return info;
    }

    private static boolean check(long timeMillis, String str) {
        String keyword = "autoResponse";
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - timeMillis > 1000 * 60 * 30) {
            //超过半小时
            return false;
        }
        String token = getMD5(timeMillis + keyword);
        return str.equals(token);
    }
}
