package com.jyzn.utils;


import com.iflytek.cloud.speech.*;
import com.jyzn.webSocketServer.FrontWebSocket;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.websocket.Session;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//tts文本转音频Pcm格式
@Component
public class TtsTool {


    //    private  String fileBase = "E:/sound";
    @Value("${fileBase}")
    private String fileBase;
    @Value("${fileBaseUrl}")
    private String FILE_BASE_URL;

    // 语音合成对象
    private SpeechSynthesizer mTts;


    private Map<String, Object> map = new HashMap<>();

    @PostConstruct
    public void init() {
        SpeechUtility.createUtility(SpeechConstant.APPID + "=" + "8b9ae7d5");
        // 初始化合成对象
        mTts = SpeechSynthesizer.createSynthesizer();
        if (mTts != null) {
            // 设置发音人
            mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
            mTts.setParameter(SpeechConstant.SPEED, "150");//设置语速
            mTts.setParameter(SpeechConstant.VOLUME, "80");//设置音量，范围0~100
            System.out.println("创建合成对象成功");

        } else {
            System.out.println("创建合成对象失败");
        }
    }

    public void textToVoice(Session session, String text) {
        try {
            // 存放音频的文件
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String date = sdf.format(new Date());
            String pcmPath = fileBase + "/" + date + ".pcm";
            // 设置合成音频保存位置（可自定义保存位置），默认不保存
            mTts.synthesizeToUri(text, pcmPath, mSynListener);
            map.put("url",FILE_BASE_URL + date + ".mp3");
            map.put("session",session);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 合成监听器
     */
    SynthesizeToUriListener mSynListener = new SynthesizeToUriListener() {

        public void onBufferProgress(int progress) {
            System.out.println("合成进度" + progress);
        }

        @SneakyThrows
        public void onSynthesizeCompleted(String uri, SpeechError error) {
            if (error == null) {
                System.out.println("合成成功");
                File f = new File(uri);
                String s = PcmToMp3.convertAudioFiles(f.getPath(), f.getPath().replace(".pcm", ".mp3"));
                FrontWebSocket.sendTTS((Session) map.get("session"),map.get("url").toString());
            } else {
                System.out.println("合成失败" + error);
            }

        }


        @Override
        public void onEvent(int eventType, int arg1, int arg2, int arg3, Object obj1, Object obj2) {
        }

    };
}

