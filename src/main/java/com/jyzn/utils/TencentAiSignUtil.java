package com.jyzn.utils;


import com.jyzn.config.Constants;
import com.jyzn.pojo.TencentAIAnswer;
import com.jyzn.service.feign.TencentAiService;
import com.jyzn.service.feign.TencentSignService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

/**
 * 腾讯开放平台相关工具类
 */


public class TencentAiSignUtil {

    private TencentAiService service;

    @Autowired
    public void setService(TencentAiService service) {
        this.service = service;
    }

    private TencentSignService signService;

    @Autowired
    public void setSignService(TencentSignService signService) {
        this.signService = signService;
    }

    /**
     * <a href="https://ai.qq.com/doc/auth.shtml">获取签名</a><br>
     * <p>
     * 签名算法<br>
     * 1. 计算步骤<br>
     * 用于计算签名的参数在不同接口之间会有差异，但算法过程固定如下4个步骤。<br>
     * <p>
     * 将<key, value>请求参数对按key进行字典升序排序，得到有序的参数对列表N<br>
     * 将列表N中的参数对按URL键值对的格式拼接成字符串，得到字符串T（如：key1=value1&key2=value2），URL键值拼接过程value部分需要URL编码，URL编码算法用大写字母，例如%E8，而不是小写%e8<br>
     * 将应用密钥以app_key为键名，组成URL键值拼接到字符串T末尾，得到字符串S（如：key1=value1&key2=value2&app_key=密钥)<br>
     * 对字符串S进行MD5运算，将得到的MD5值所有字符转换成大写，得到接口请求签名<br>
     * 2. 注意事项<br>
     * 不同接口要求的参数对不一样，计算签名使用的参数对也不一样<br>
     * 参数名区分大小写，参数值为空不参与签名<br>
     * URL键值拼接过程value部分需要URL编码<br>
     * 签名有效期5分钟，需要请求接口时刻实时计算签名信息<br>
     * 更多注意事项，请查看<a href="https://ai.qq.com/doc/faq.shtml">常见问题</a>
     *
     * @return 签名字符串
     */
    public static Map<String, String> getReqSign(Map<String, String> params) throws IOException {
        // 先将参数以其参数名的字典序升序进行排序
        Map<String, String> sortedParams = new TreeMap<>(params);
        Set<Map.Entry<String, String>> entrys = sortedParams.entrySet();
        // 遍历排序后的字典，将所有参数按"key=value"格式拼接在一起
        StringBuilder baseString = new StringBuilder();
        for (Map.Entry<String, String> param : entrys) {
            //sign参数 和 空值参数 不加入算法
            if (param.getValue() != null && !"".equals(param.getKey().trim()) && !"sign".equals(param.getKey().trim()) && !"".equals(param.getValue().trim())) {
                baseString.append(param.getKey().trim()).append("=").append(URLEncoder.encode(param.getValue().trim(), "UTF-8")).append("&");
            }
        }
        System.err.println("未拼接APPKEY的参数：" + baseString.toString());
        if (baseString.length() > 0) {
            baseString.deleteCharAt(baseString.length() - 1).append("&app_key=" + Constants.TencentAi.APP_KEY);
        }
        System.err.println("拼接APPKEY后的参数：" + baseString.toString());
        // 使用MD5对待签名串求签
        try {
            String sign = com.jyzn.utils.Md5Util.getMD5(baseString.toString()).toUpperCase();
            System.out.println(sign);
            sortedParams.put("sign", sign);
        } catch (Exception ex) {
            throw new IOException(ex);
        }
        return sortedParams;

    }

    public static Map<String, String> getSignMap(String userName, String msg) {
        Map<String, String> tempParamS = new HashMap<>();
        Map<String, String> signMap = new HashMap<>();
        try {
            tempParamS.put("app_id", Constants.TencentAi.APP_ID);
            tempParamS.put("time_stamp", new Date().getTime() / 1000 + "");
            tempParamS.put("nonce_str", com.jyzn.utils.RandomUtils.generateString(16));
            tempParamS.put("session", userName);
//            tempParamS.put("session", RandomUtils.getNotSimple(6) + "");
            tempParamS.put("question", msg);
            tempParamS.put("sign", "");
            signMap = getReqSign(tempParamS);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return signMap;
    }

    public static String getAnswer(TencentAIAnswer answer) {
        String result;
        if (answer.getRet() != 0) {
            result = "不好意思，我刚才开了个小差。你能再说一遍吗？";
        } else {
            result = answer.getData().getAnswer();
        }
        return result;
    }

}
