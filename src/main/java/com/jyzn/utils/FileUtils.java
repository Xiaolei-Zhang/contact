package com.jyzn.utils;

import com.jyzn.pojo.bo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
public class FileUtils {

    @Value("${fileBase}")
    private String fileBase;

    public  Result uploadFile(MultipartFile f) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String date = sdf.format(new Date());
        if (f == null) {
            return ResultUtil.error(1, "file is null");
        }
        File folder = new File(fileBase);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        String fileName = f.getOriginalFilename();
        log.info(fileName);
        log.info(f.getSize() + "");
        log.info(String.valueOf(f.isEmpty()));
        BufferedOutputStream bos = null;
        try {
//            f.transferTo(target);
//            String path = "temp/file/" + unionId + "/" + name + "/";
//            path = path + fileName;
            bos = new BufferedOutputStream(new FileOutputStream(folder + "/" + date + fileName));
            bos.write(f.getBytes());
            return ResultUtil.success(folder + "/" + date + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return ResultUtil.error(1, "上传失败");
    }


}
