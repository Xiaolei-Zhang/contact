package com.jyzn.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Future;

import com.jyzn.webSocketServer.FrontWebSocket;
import com.microsoft.cognitiveservices.speech.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.websocket.Session;

@Component
@Slf4j
public class MrisoftTTS {

//    @Value("${fileBase}")
//    private  String fileBase;

    private String fileBase = "E:/dataHigh/";

    private  final String FILE_BASE_URL = "http://api.hzjyzn.cn:8061/";

    public  void tts(Session session,String text) throws FileNotFoundException {

        // 存放音频的文件
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String date = sdf.format(new Date());
        File f = new File(fileBase + "/" + date + ".pcm");
        String fileUrl = FILE_BASE_URL + date + ".mp3";
        FileOutputStream os = new FileOutputStream(f);
        String speechSubscriptionKey = "4ede77ca04ca4dc6b80f9fe1272fc1ce";
        String serviceRegion = "eastasia";
        SpeechConfig config = SpeechConfig.fromSubscription(speechSubscriptionKey, serviceRegion);
        config.setSpeechSynthesisLanguage("zh-CN");
        config.setSpeechSynthesisOutputFormat(SpeechSynthesisOutputFormat.Raw24Khz16BitMonoPcm);
        try (
                SpeechSynthesizer synth = new SpeechSynthesizer(config)) {

            assert(config != null);
            assert(synth != null);

            int exitCode = 1;

            SpeechSynthesisResult result = synth.SpeakTextAsync(text).get();
            Future<SynthesisVoicesResult> future=synth.getVoicesAsync();
            AudioDataStream audioDataStream=AudioDataStream.fromResult(result);
            byte[] data =new byte[1000000];
            audioDataStream.readData(data);
            SynthesisVoicesResult r=future.get();

            os.write(data);
            os.flush();

            PcmToMp3.convertAudioFiles(f.getPath(), f.getPath().replace(".pcm", ".mp3"));
            FrontWebSocket.sendTTS(session,fileUrl);
            log.info(fileUrl);
            assert (r!=null);
            assert(result != null);

            if (result.getReason() == ResultReason.SynthesizingAudioCompleted) {
                System.out.println("Speech synthesized to speaker for text [" + text + "]");
                exitCode = 0;
            }
            else if (result.getReason() == ResultReason.Canceled) {
                SpeechSynthesisCancellationDetails cancellation = SpeechSynthesisCancellationDetails.fromResult(result);
                System.out.println("CANCELED: Reason=" + cancellation.getReason());

                if (cancellation.getReason() == CancellationReason.Error) {
                    System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
                    System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
                    System.out.println("CANCELED: Did you update the subscription info?");
                }
            }

            System.exit(exitCode);
        } catch (Exception ex) {
            System.out.println("Unexpected exception: " + ex.getMessage());

            assert(false);
            System.exit(1);
        }
    }

}
