package com.jyzn.controller;

import com.jyzn.pojo.OCRResult;
import com.jyzn.pojo.bo.Result;
import com.jyzn.service.OcrService;
import com.jyzn.utils.AsrUtils.AsrMain;
import com.jyzn.utils.AsrUtils.DemoException;
import com.jyzn.utils.PcmToMp3;
import com.jyzn.utils.ResultUtil;
import com.jyzn.utils.TTSUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/hello")
@Api(value = "hello controller", tags = {"测试接口"})
public class HelloController extends BaseController {

    @Resource
    private TTSUtils ttsUtils;

    private OcrService ocrService;

    @Autowired
    public void setOcrService(OcrService ocrService) {
        this.ocrService = ocrService;
    }


    @ApiOperation(value = "音频文件上传接口", notes = "音频文件上传接口")
    @PostMapping("/uploadMode")
    public Result uploadMode(String filePath) {
        log.info("开始ASR");
        AsrMain demo = new AsrMain();
        try {
            String text = AsrMain.asrStart(filePath);
            return ResultUtil.success(text);
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(e.toString());
        } catch (DemoException e) {
            e.printStackTrace();
            return ResultUtil.error(e.toString());
        }

    }

    @ApiOperation(value = "OCR测试接口", notes = "OCR测试接口")
    @PostMapping("/OCRTest")
    public void OCRTest(String imgUrl) {
        log.info("OCR START");
        OCRResult res = ocrService.OCR(imgUrl);
        if (res != null) {
            String resWord = "";
            List<Map<String, String>> wordsList = res.getWords_result();
            for (Map<String, String> word : wordsList) {
                resWord = resWord + word.get("words");
            }
            log.info(resWord);
        }
    }

    @ApiOperation(value = "TTS测试接口", notes = "TTS测试接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "accessToken", value = "token", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/TTSTest")
    public void TTSTest(String userId, String text) {
        try {
            ttsUtils.TTS(userId, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "音频转换工具", notes = "音频转换工具")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "filePath", value = "文件路径", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "channels", value = "通道数", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "samplesPerSec", value = "采样率", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/PCM2MP3")
    public void PCM2MP3(String filePath, Short channels, Integer samplesPerSec) {
        try {
            PcmToMp3.convertAudioFilesWithMyArgs(filePath, filePath.replace(".pcm", ".mp3"), channels, samplesPerSec);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
