package com.jyzn.controller;

import com.jyzn.pojo.Contact;
import com.jyzn.pojo.bo.Result;
import com.jyzn.service.ContactService;
import com.jyzn.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/contact")
@Api(value = "contact controller", tags = {"联系我们"})
public class ContactController extends BaseController {

    @Resource
    private ContactService contactService;

    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名" ,dataType = "String",required = true),
            @ApiImplicitParam(name = "phone", value = "手机",dataType = "String",required = true),
            @ApiImplicitParam(name = "email", value = "邮箱",dataType = "String"),
            @ApiImplicitParam(name = "company", value = "公司",dataType = "String"),
    })
    @ApiOperation("添加用户信息")
    @PostMapping("/save")
    public ResponseEntity<Result> add(String name,String phone,String email,String company){
        Contact contact = new Contact();
        contact.setName(name);
        contact.setPhone(phone);
        contact.setEmail(email);
        contact.setCompany(company);
        Integer save = contactService.add(contact);
        if(save>0){
            return ResultUtil.success(save).toResponseEntity(HttpStatus.OK);
        }else {
            return ResultUtil.error("保存失败").toResponseEntity();
        }

    }

}
