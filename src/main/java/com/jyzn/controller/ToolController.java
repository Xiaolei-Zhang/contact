package com.jyzn.controller;


import com.jyzn.service.InputTransService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/tool")
@Api(value = "tool controller", tags = {"工具接口"})
public class ToolController extends BaseController {

    private InputTransService inputTransService;

    @Autowired
    public void setInputTransService(InputTransService inputTransService) {
        this.inputTransService = inputTransService;
    }

    @ApiOperation(value = "工具调用接口", notes = "工具调用接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "skillId", value = "技能ID（1:原生 2:闲聊 3:法律咨询）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "inputId", value = "输入方式ID(1:文本 2:图片链接 3:图片上传 4:音频链接)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "outputId", value = "输出方式ID(1:文本 2:TTS)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "content ", value = "内容", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "userId ", value = "用户ID(用于长连接推送)", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/basetool")
    public void basetool(Integer skillId, Integer inputId, Integer outputId, String content, String userId) {
        inputTransService.inputTrans(skillId, inputId, outputId, content, userId);
    }

    @ApiOperation(value = "工具调用接口(文件上传)", notes = "工具调用接口(文件上传)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "skillId", value = "技能ID（1:原生 2:闲聊 3:法律咨询）", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "inputId", value = "输入方式ID(1:文本 2:图片链接 3:图片上传 4:音频链接)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "outputId", value = "输出方式ID(1:文本 2:TTS)", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "content ", value = "内容", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "userId ", value = "用户ID(用于长连接推送)", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/basetoolWithFile")
    public void basetoolWithFile(@RequestParam("file") MultipartFile files, Integer skillId, Integer inputId, Integer outputId, String userId) {
        inputTransService.inputTransWithFile(files, skillId, inputId, outputId, userId);
    }
}
