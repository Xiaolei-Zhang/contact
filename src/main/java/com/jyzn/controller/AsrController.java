package com.jyzn.controller;

import com.jyzn.pojo.bo.Result;
import com.jyzn.utils.AsrUtils.AsrMain;
import com.jyzn.utils.AsrUtils.DemoException;
import com.jyzn.utils.FileUtils;
import com.jyzn.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/asr")
@Api(value = "asr controller", tags = {"asr接口"})
public class AsrController extends BaseController {

    @Resource
    private FileUtils fileUtils;


    @ApiOperation(value = "音频文件上传接口", notes = "音频文件上传接口")
    @PostMapping("/uploadMode")
    public Result uploadMode(HttpServletRequest request, @RequestParam("file") MultipartFile files) {
        Result fileUpload = fileUtils.uploadFile(files);
        log.info(fileUpload.getCode().toString());
        if (fileUpload.getCode() == 0) {
            log.info("开始ASR");
            try {
                String text = AsrMain.asrStart((String) fileUpload.getData());
                return ResultUtil.success(text);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DemoException e) {
                e.printStackTrace();
            }
        }
        return fileUpload;
    }

}
