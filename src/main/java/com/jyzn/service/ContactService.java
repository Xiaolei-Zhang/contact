package com.jyzn.service;

import com.jyzn.pojo.Contact;

public interface ContactService extends BaseService<Contact> {

    public Integer add(Contact contact);
}
