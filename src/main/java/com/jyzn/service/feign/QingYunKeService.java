package com.jyzn.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "qingyunke", url = "http://api.qingyunke.com/")
public interface QingYunKeService {

    @GetMapping("api.php")
    String qingYunKe(@RequestParam String key, @RequestParam Integer appid, @RequestParam String msg);
}
