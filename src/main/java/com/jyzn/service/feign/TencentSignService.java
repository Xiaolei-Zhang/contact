package com.jyzn.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "tencentSign", url = "https://api.ai.qq.com/path/")
public interface TencentSignService {

    @PostMapping("to/api")
    String test(@RequestParam Map<String, String> signMap);
}
