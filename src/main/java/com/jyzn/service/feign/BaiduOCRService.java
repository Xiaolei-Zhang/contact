package com.jyzn.service.feign;

import com.jyzn.pojo.OCRResult;
import com.jyzn.pojo.TokenResult;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "BDOCR", url = "https://aip.baidubce.com/")
public interface BaiduOCRService {

    @PostMapping("oauth/2.0/token")
    TokenResult getToken(@RequestParam String grant_type, @RequestParam String client_id, @RequestParam String client_secret);

    @PostMapping(value = "rest/2.0/ocr/v1/general_basic", headers = {"content-type=application/x-www-form-urlencoded"})
    @Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    OCRResult getGeneralOCRResult(@RequestParam String access_token, @RequestBody String image);

    @PostMapping(value = "rest/2.0/ocr/v1/accurate_basic", headers = {"content-type=application/x-www-form-urlencoded"})
    @Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    OCRResult getAccurateOCRResult(@RequestParam String access_token, @RequestBody String image);

    @PostMapping(value = "rest/2.0/solution/v1/iocr/recognise")
    @Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    String getIOCRResult(@RequestParam String access_token, @RequestBody String image);


}
