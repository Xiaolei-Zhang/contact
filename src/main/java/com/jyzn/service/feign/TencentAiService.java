package com.jyzn.service.feign;

import com.jyzn.pojo.TencentAIAnswer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "tencentAi", url = "https://api.ai.qq.com/fcgi-bin/")
public interface TencentAiService {

    @PostMapping("nlp/nlp_textchat")
    TencentAIAnswer textChat(@RequestParam Map<String, String> signMap);
}
