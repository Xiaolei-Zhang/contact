package com.jyzn.service;

import org.springframework.web.multipart.MultipartFile;

public interface InputTransService {

    void inputTrans(Integer skillId, Integer inputId, Integer outputId, String content, String userId);

    void inputTransWithFile(MultipartFile files, Integer skillId, Integer inputId, Integer outputId, String userId);

}
