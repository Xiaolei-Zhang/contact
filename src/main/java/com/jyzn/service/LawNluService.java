package com.jyzn.service;

import org.springframework.http.ResponseEntity;

public interface LawNluService {

    ResponseEntity getKeyWord(String sentence);

}
