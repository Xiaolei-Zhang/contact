package com.jyzn.service;

import com.jyzn.pojo.OCRResult;
import org.springframework.web.multipart.MultipartFile;

public interface OcrService {

    OCRResult OCR(String imgUrl);

    OCRResult OCRFile(MultipartFile file);

}
