package com.jyzn.service.impl;

import com.jyzn.pojo.KeyWord;
import com.jyzn.pojo.LawReply;
import com.jyzn.pojo.NlgAnswer;
import com.jyzn.pojo.TencentAIAnswer;
import com.jyzn.service.NluService;
import com.jyzn.service.OutputService;
import com.jyzn.service.SkillService;
import com.jyzn.service.feign.FeignService;
import com.jyzn.service.feign.TencentAiService;
import com.jyzn.utils.TencentAiSignUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Service
public class SkillServiceImpl implements SkillService {

    private NluService nluService;

    @Resource
    public void setNluService(NluService nluService) {
        this.nluService = nluService;
    }

    private OutputService outputService;

    @Resource
    public void setOutputService(OutputService outputService) {
        this.outputService = outputService;
    }

    private TencentAiService service;

    @Resource
    public void setService(TencentAiService service) {
        this.service = service;
    }

    private FeignService feignService;

    @Resource
    public void setFeignService(FeignService feignService) {
        this.feignService = feignService;
    }

    @Override
    public void skill(Integer skillId, Integer outputId, String content, String userId) {
        switch (skillId) {
            case 1:
                break;
            case 2:
                NlgAnswer answer = nluService.nlgResponse(content);
                if (answer.getSuccess() && !StringUtils.isEmpty(answer.getData().getReply())) {
                    content = answer.getData().getReply();
                } else {
                    Map<String, String> signMap = TencentAiSignUtil.getSignMap(userId, content);
                    TencentAIAnswer tencentAIAnswer = service.textChat(signMap);
                    content = TencentAiSignUtil.getAnswer(tencentAIAnswer);
                }
                break;
            case 3:
                KeyWord keyWordData = feignService.getKeywords(content, 1);
                if (keyWordData.getData() != null) {
                    String keyWord = keyWordData.getData().get(0);
                    log.info("keyword:" + keyWord);
                    LawReply legalAdvice = feignService.legalAdvice(keyWord);
                    if (legalAdvice.getSuccess()) {
                        content = legalAdvice.getData().get("reply");
                    }
                }
                break;
        }
        outputService.output(outputId, content, userId);


    }
}
