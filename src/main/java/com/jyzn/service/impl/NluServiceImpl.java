package com.jyzn.service.impl;

import com.jyzn.pojo.NlgAnswer;
import com.jyzn.service.NluService;
import com.jyzn.service.feign.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NluServiceImpl implements NluService {

    private FeignService service;

    @Autowired
    public void setService(FeignService service) {
        this.service = service;
    }

    @Override
    public NlgAnswer nlgResponse(String sentence) {
        return service.nlgResponse(sentence);
    }
}
