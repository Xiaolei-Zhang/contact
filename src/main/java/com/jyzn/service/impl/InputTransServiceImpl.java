package com.jyzn.service.impl;

import com.jyzn.pojo.OCRResult;
import com.jyzn.service.InputTransService;
import com.jyzn.service.OcrService;
import com.jyzn.service.SkillService;
import com.jyzn.utils.AsrUtils.AsrMain;
import com.jyzn.utils.AsrUtils.DemoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class InputTransServiceImpl implements InputTransService {

    private SkillService skillService;

    @Autowired
    public void setSkillService(SkillService skillService) {
        this.skillService = skillService;
    }

    private OcrService ocrService;

    @Autowired
    public void setOcrService(OcrService ocrService) {
        this.ocrService = ocrService;
    }

    @Override
    public void inputTrans(Integer skillId, Integer inputId, Integer outputId, String content, String userId) {
        switch (inputId) {
            // ASR
            case 1:
                break;
            // OCR
            case 2:
                OCRResult res = ocrService.OCR(content);
                if (res != null) {
                    String resWord = "";
                    List<Map<String, String>> wordsList = res.getWords_result();
                    for (Map<String, String> word : wordsList) {
                        resWord = resWord + word.get("words");
                    }
                    content = resWord;
                } else {
                    content = "看不懂你发的东西";
                }
                break;
            case 3:
                try {
                    content = AsrMain.asrStart(content);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DemoException e) {
                    e.printStackTrace();
                }
                break;
        }
        skillService.skill(skillId, outputId, content, userId);
    }

    @Override
    public void inputTransWithFile(MultipartFile file, Integer skillId, Integer inputId, Integer outputId, String userId) {
        String content = "我没理解你发的";
        switch (inputId) {
            case 11:
                OCRResult res =  ocrService.OCRFile(file);
                if (res != null) {
                    String resWord = "";
                    List<Map<String, String>> wordsList = res.getWords_result();
                    for (Map<String, String> word : wordsList) {
                        resWord = resWord + word.get("words");
                    }
                    content = resWord;
                } else {
                    content = "看不懂你发的东西";
                }
                break;
        }
        skillService.skill(skillId, outputId, content, userId);
    }
}
