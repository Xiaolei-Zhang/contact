package com.jyzn.service.impl;

import com.jyzn.pojo.OCRResult;
import com.jyzn.pojo.TokenResult;
import com.jyzn.service.OcrService;
import com.jyzn.service.feign.BaiduOCRService;
import com.jyzn.utils.Base64Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.net.URLEncoder;

import static com.jyzn.config.Constants.BDOCR.*;

@Slf4j
@Service
public class OcrServiceImpl implements OcrService {

    private BaiduOCRService service;

    @Resource
    public void setService(BaiduOCRService service) {
        this.service = service;
    }

    @Override
    public OCRResult OCR(String imgUrl) {
        TokenResult token = service.getToken(GRANT_TYPE, API_KEY, SECRET_KEY);
        String accessToken = token.getAccess_token();
        log.info(accessToken);
        try {
//            byte[] imgData = file.getBytes();
//            String imgStr = Base64Util.encode(imgData);
//            String imgParam = URLEncoder.encode(imgStr, "UTF-8");
//            String param = "image=" + imgParam + "&detect_language=true&detect_direction=true";
            imgUrl = imgUrl.replace("https://", "http://");
            String param = "url=" + imgUrl + "&detect_language=true&detect_direction=true";
            return service.getGeneralOCRResult(accessToken, param);
//            System.out.println(ocrResult);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error", e);
            return null;
        }
    }

    @Override
    public OCRResult OCRFile(MultipartFile file) {
        TokenResult tokenResult = service.getToken(GRANT_TYPE, API_KEY, SECRET_KEY);
        String accessToken = tokenResult.getAccess_token();
        log.info(accessToken);
        try {
            byte[] imgData = file.getBytes();
            String imgStr = Base64Util.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");
            String param = "image=" + imgParam + "&detect_language=true&detect_direction=true";
            return service.getGeneralOCRResult(accessToken, param);
//            System.out.println(ocrResult);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("error", e);
            return null;
        }
    }
}
