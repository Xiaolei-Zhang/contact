package com.jyzn.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyzn.mapper.ContactMapper;
import com.jyzn.pojo.Contact;
import com.jyzn.service.ContactService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

@Service
public class ContactServiceImpl implements ContactService {

    @Resource
    private ContactMapper contactMapper;

    @Override
    public boolean saveBatch(Collection<Contact> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean save(Contact entity) {
        return false;
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Contact> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean updateBatchById(Collection<Contact> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdate(Contact entity) {
        return false;
    }

    @Override
    public Contact getOne(Wrapper<Contact> queryWrapper, boolean throwEx) {
        return null;
    }

    @Override
    public Map<String, Object> getMap(Wrapper<Contact> queryWrapper) {
        return null;
    }

    @Override
    public <V> V getObj(Wrapper<Contact> queryWrapper, Function<? super Object, V> mapper) {
        return null;
    }

    @Override
    public BaseMapper<Contact> getBaseMapper() {
        return null;
    }

    @Override
    public Integer add(Contact contact) {
        int insert = contactMapper.insert(contact);
        return insert;
    }

    @Override
    public boolean removeById(Serializable id) {
        return false;
    }
}
