package com.jyzn.service.impl;

import com.jyzn.service.OutputService;
import com.jyzn.utils.TTSUtils;
import com.jyzn.webSocketServer.WebSocketServer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service
public class OutputServiceImpl implements OutputService {

    @Resource
    private TTSUtils ttsUtils;

    @Override
    public void output(Integer outputId, String content, String userId) {
        switch (outputId) {
            case 1:
                try {
                    WebSocketServer.sendInfo(content, userId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    ttsUtils.TTS(userId, content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
