package com.jyzn.service;

public interface OutputService {

    void output(Integer outputId, String content, String userId);
}
