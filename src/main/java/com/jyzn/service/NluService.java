package com.jyzn.service;

import com.jyzn.pojo.NlgAnswer;

public interface NluService {
    NlgAnswer nlgResponse(String sentence);
}
